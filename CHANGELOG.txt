- You can now delete and restore patients.
- Added mechanisms to deploy shared patient rooms. (Coming in future versions)
- General Improvements
- Added a help menu
- Improved lab Results Input. Better Layout.
- Added an Iphone version?