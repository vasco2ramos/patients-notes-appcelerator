if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}
else {
	// This is a single context application with multiple windows in a stack
	(function() {
		//determine platform and form factor and render appropriate components
		var osname = Ti.Platform.osname,
			height = Ti.Platform.displayCaps.platformHeight,
			width = Ti.Platform.displayCaps.platformWidth;
	
		
		var db = Ti.Database.install('patientnotes.sqlite', 'pnotes');
		db.file.setRemoteBackup(false);
		
		var currentVersion = Ti.App.version;
		var cachedVersion = Ti.App.Properties.getString("version");
		if(cachedVersion == null){
			//check stuff
			try{
				var row = db.execute("SELECT * FROM version;");
				if(row.isValidRow()){
					cachedVersion = row.fieldByName('number');
					Ti.App.Properties.setString("version", currentVersion);
				}
			}
			catch(e){
				cachedVersion = "1.0.0";
			}
		}
		if(cachedVersion=="1.0.0"){
			//installNewDatabase();
			Ti.API.info("installing new database");
			db.execute("ALTER TABLE patients ADD COLUMN sex char(1);");
			db.execute("CREATE  TABLE version (number VARCHAR(10) PRIMARY KEY  NOT NULL );");
			db.execute("INSERT INTO version (number) values (?);",currentVersion);
			Ti.App.Properties.setString("version", currentVersion);
		}	
		
		//considering tablet to have one dimension over 900px - can define your own
		var isTablet = osname === 'ipad' || (osname === 'android' && (width > 899 || height > 899));
	
		var Window;
		if (isTablet) {
			Window = require('ui/tablet/ApplicationWindow');
			Window.orientationModes = [
				Titanium.UI.LANDSCAPE_LEFT,
				Titanium.UI.LANDSCAPE_RIGHT,
				Titanium.UI.PORTRAIT, 
    			Titanium.UI.UPSIDE_PORTRAIT,
			];
		}
		else {
			if (osname === 'android') {
				Window = require('ui/handheld/android/ApplicationWindow');
			}
			else if (osname === 'mobileweb') {
				Window = require('ui/handheld/mobileweb/ApplicationWindow');
			}
			else {
				Window = require('ui/handheld/ios/ApplicationWindow');
			}
		} 
		Ti.UI.setBackgroundColor('#fff');
		new Window().open();
	//	Ti.UI.orientation = Ti.UI.LANDSCAPE_LEFT;
	})();
}
