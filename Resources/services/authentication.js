module.exports.authUser = function(mail,pass){
	var xhr2 = Titanium.Network.createHTTPClient(); 
	xhr2.onload = function(){
    	var json = this.responseText;  
	    var response = JSON.parse(json);  
	    if (response.logged == true)  {  
	        alert("Welcome " + response.name + ". Your email is: " + mail);
	        Ti.App.fireEvent('grantAccess', { email:mail, name: response.name});  
	    }  
	    else  {  
	        alert(response.message);  
    	}
	};
	xhr2.onerror = function(){
    	Ti.API.info('in utf-8 error for POST');
	}; 
	xhr2.open("POST","https://patientsnotes.fluiddo.com/members/process_login.php");  
    var params = {  
		email: mail,  
		p: pass 
	};  
	xhr2.send(params); 
};


module.exports.sendPatient = function(receiverEmail, patient, notes, pass){
	var xhr2 = Titanium.Network.createHTTPClient(); 
	xhr2.onload = function(){
       	var json = this.responseText;
       	alert(json);
    	var response = JSON.parse(json);  
	    if (response.operation == true)  {  
	        alert(response.message);  
	    }  
	    else  {  
	        alert(response.message);  
    	}
	};
	xhr2.onerror = function(){
    	Ti.API.info('in utf-8 error for POST');
	}; 
	xhr2.open("POST","https://patientsnotes.fluiddo.com/members/process_patient.php");  
   	var params = {  
		receiver: receiverEmail,
		name: patient.name, 
   		birthDate: patient.birthdate, 
   		admissionDate: patient.admissiondate,
   		reason: patient.reason, 
   		room: patient.room,
   		bed: patient.bed, 
   		createdDate: patient.createddate,
   		modifiedDate: patient.modifieddate, 
   		state:patient.state, 
   		sex: patient.sex,
   		notes: JSON.stringify(notes),
   		password: pass,
	};  
	xhr2.send(params); 
};



module.exports.getSharedPatient = function(id, pass){	
	var xhr2 = Titanium.Network.createHTTPClient(); 
	xhr2.onload = function(){
       	var json = this.responseText;
       	alert(json);
       	var response = JSON.parse(json);
       	if(response.operation == false){
       		alert(response.message);
       		return false;
       	}
       	else{
       		Ti.App.fireEvent('continue2', {response:response});	
       	}
       	return response;
	};
	xhr2.onerror = function(){
    	Ti.API.info('in utf-8 error for POST');
	}; 
	xhr2.open("POST","https://patientsnotes.fluiddo.com/members/get_shared_patient.php");
	var params = {  
		id: id,
		password: pass,
	}; 
	xhr2.send(params); 
};

module.exports.getPatients = function(){	
	var xhr2 = Titanium.Network.createHTTPClient(); 
	xhr2.onload = function(){
       	var json = this.responseText;
       	var response = JSON.parse(json);
       	Ti.App.fireEvent('continue1', {response:response});
       	return response;
	};
	xhr2.onerror = function(){
    	Ti.API.info('in utf-8 error for POST');
	}; 
	xhr2.open("GET","https://patientsnotes.fluiddo.com/members/receive_patient.php");  
	xhr2.send(); 
};





module.exports.sendNotes = function(patientId,notes){	
	var xhr2 = Titanium.Network.createHTTPClient(); 
	alert('sending notes');
	xhr2.onload = function(){
    	var json = this.responseText;
    	//var response = JSON.parse(json);
    	alert(json);    
	};
	xhr2.onerror = function(){
    	Ti.API.info('in utf-8 error for POST');
	}; 
	xhr2.open("POST","https://patientsnotes.fluiddo.com/members/process_note.php");  
    var params = {  
		notes: JSON.stringify(notes),
	};  
	xhr2.send(params); 
};