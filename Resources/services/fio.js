/**
 * @author Vasco Ramos
 */
 
var DATABASE_NAME = 'pnotes';

/******************************
 ***  PATIENTS OPERATIONS ****
 ******************************/

/*
 * Gets all patients
 */

module.exports.getPatients = function(){
	Ti.API.info("Getting All Patients");
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM patients;';
	var rows = db.execute(sql);
	var arr = [];
  	while(rows.isValidRow()) {
    	var obj = {};
    	for(i=0; i<rows.fieldCount(); i++) {
      		obj[rows.fieldName(i)] = rows.field(i);
  	  	}
  	  	Ti.API.info("---"+obj+"---");
  	  	arr.push(obj);
 		rows.next();
  	}
	db.close();
	Ti.API.info("DONE Getting All Patients");
	return arr;
};

/*
 * Gets a patient by id
 */
module.exports.getPatientById = function(id){
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM patients WHERE id = ?;';
	var rows = db.execute(sql, id);
	var arr = [];
  	if(rows.isValidRow()) {
    	var obj = {};
    	for(i=0; i<rows.fieldCount(); i++) {
      		obj[rows.fieldName(i)] = rows.field(i);
  	  	}
  	  	db.close();
 		return obj;
  	}
};


/*
 * Adds a new patient to json object
 */
module.exports.addPatient = function(patient){
	Ti.API.info("Adding New Patient");
	var db = Ti.Database.open(DATABASE_NAME);
	var nascimento = patient.birthdate;
	nascimento = getDate(nascimento);
	var admissao = patient.admissiondate;
	admissao = getDate(admissao);
	var sql = 'INSERT INTO patients (name, birthdate, admissiondate, reason, room, bed, state, sex)';
	sql += 'values (?, date(?),date(?),?,?,?,?,?);';
	var rows = db.execute(sql, patient.name, nascimento, admissao, patient.reason, patient.room, patient.bed, patient.state,patient.sex);
	db.close();	
};


/*
 * Updates patient
 */
module.exports.updatePatient = function(patient, _id){
	Ti.API.info("Updating Patient");
	var db = Ti.Database.open(DATABASE_NAME);
	var nascimento = patient.birthdate;
	nascimento = getDate(nascimento);
	var admissao = patient.admissiondate;
	admissao = getDate(admissao);
	var sql = 'UPDATE patients SET name=?, birthdate=date(?), admissiondate=date(?), reason=?, room=?, bed=?, state=?, sex=? where id = ?;';
	db.execute(sql, patient.name, nascimento, admissao, patient.reason, patient.room, patient.bed, patient.state, patient.sex, patient.id);
	db.close();
};

/*
 * Delete patient
 */
module.exports.deletePatient = function(id){
	Ti.API.log('Deleting Patient ID - '+id);
	var mydb = Ti.Database.open(DATABASE_NAME);
	mydb.execute('DELETE FROM patients where id = ?;', id);
	mydb.close();
};

/*
 * Delete all patients from room X
 */
module.exports.deletePatientsByRoom = function(room){
	Ti.API.log('Deleting Patients from Room - '+ room);
	var db = Ti.Database.open(DATABASE_NAME);
	var sql1 = 'SELECT * FROM patients WHERE room = ?;';
	var sql2 = 'DELETE FROM notes WHERE patientId = ?;';
	var sql3 = 'DELETE FROM patients where room = ?;';
	var rows = db.execute(sql1, room);
  	if(rows.isValidRow()) {	
  		Ti.API.log('Deleting Patient ID - ', rows.field(i));
    	db.execute(sql2, rows.field(i));
  	}
	db.execute(sql3, room);
	db.close();
};


/*
 * Delete patient by room and bed
 */
module.exports.deletePatientByLocal = function(room, bed){
	Ti.API.log('Deleting Patient ID - '+id);
	var mydb = Ti.Database.open(DATABASE_NAME);
	mydb.execute('DELETE FROM patients where room = ? & bed = ?;', id,bed);
	mydb.close();
};


/******************************
 *****  NOTES OPERATIONS ******
 ******************************/

/*
 * Gets all notes
 */

module.exports.getNotes = function(){
	Ti.API.info("Getting All Notes");
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM notes;';
	var rows = db.execute(sql);
	var arr = [];
  	while(rows.isValidRow()) {
    	var obj = {};
    	for(i=0; i<rows.fieldCount(); i++) {
      		obj[rows.fieldName(i)] = rows.field(i);
  	  	}
  	  	Ti.API.info("---"+obj+"---");
  	  	arr.push(obj);
 		rows.next();
  	}
	db.close();
	Ti.API.info("DONE Getting All Notes");
	return arr;
};

module.exports.getNotesById = function(patientId){
	Ti.API.info("Getting all notes of patient - "+patientId);
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM notes WHERE patientId = ?;';
	var rows = db.execute(sql, patientId);
	var arr = [];
  	while(rows.isValidRow()) {
    	var obj = {};
    	for(i=0; i<rows.fieldCount(); i++) {
      		obj[rows.fieldName(i)] = rows.field(i);
  	  	}
  	  	Ti.API.info("---"+obj+"---");
  	  	arr.push(obj);
 		rows.next();
  	}
	db.close();
	Ti.API.info("DONE Getting All Notes");
	return arr;
};

module.exports.addNote = function(patientId, note){
	Ti.API.info("Adding New Note to patient - "+patientId);
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'INSERT INTO notes (patientId, text, na, k, cl,urea,crea,lact,leuco,hgb,ht,alt,ast,plaq,vanc,valp,pcr)';
	sql += 'values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
	var rows = db.execute(sql, patientId, note.text, note.na, note.k, note.cl, note.urea, note.crea, note.lact, note.leuco, note.hgb, note.ht, note.alt, note.ast, note.plaq, note.vanc, note.valp, note.pcr);
	db.close();	
};

module.exports.deleteNotesById = function(id){
	Ti.API.log('Deleting Notes from Patient ID - '+id);
	var mydb = Ti.Database.open(DATABASE_NAME);
	mydb.execute('DELETE FROM notes where patientId = ?;', id);
	mydb.close();
};

/******************************
 ******  ROOMS OPERATIONS *****
 ******************************/

// Gets number of beds per room only
module.exports.getOnlyRooms = function(){
	Ti.API.info("Getting all Rooms");
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM rooms;';
	var rows = db.execute(sql);
	var arr = [];
  	while(rows.isValidRow()) {
  	  	arr.push(rows.field(2));
 		rows.next();
  	}
	db.close();
	Ti.API.info("DONE Getting All Rooms - "+arr);
	return arr;
};

// Gets Everything from rooms
module.exports.getRooms = function(){
	Ti.API.info("Getting all Rooms");
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM rooms;';
	var rows = db.execute(sql);
	var arr = [];
  	while(rows.isValidRow()) {
    	var obj = {};
    	for(i=0; i<rows.fieldCount(); i++) {
      		obj[rows.fieldName(i)] = rows.field(i);
  	  	}
  	  	Ti.API.info("---"+obj+"---");
  	  	arr.push(obj);
 		rows.next();
  	}
	db.close();
	Ti.API.info("DONE Getting All Rooms - "+arr);
	return arr;
};


module.exports.deleteRoom = function(id){
	Ti.API.log('Deleting Room ID - '+id);
	var mydb = Ti.Database.open(DATABASE_NAME);
	mydb.execute('DELETE FROM rooms where id = ?;', id);
	mydb.close();
};


module.exports.createRoom = function(beds){
	Ti.API.log('Creating Room ID');
	var mydb = Ti.Database.open(DATABASE_NAME);
	var sql = 'INSERT INTO rooms (name,beds) values(?,?);';
	mydb.execute(sql, 'Teste', beds);
	mydb.close();
};

/******************************
 *****  GENERAL OPERATIONS ****
 ******************************/

module.exports.getNumberOfRooms = function(){
	var mydb = Ti.Database.open(DATABASE_NAME);
    var row = mydb.execute('SELECT count(*) as nrooms from rooms;');
    Ti.API.info(row.fieldByName('nrooms'));
    return row.fieldByName('nrooms');
};

module.exports.getNumberOfBeds = function(roomNumber){
	var mydb = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT beds as nbeds from rooms WHERE id = ?;';
    var row = mydb.execute(sql,roomNumber);
    Ti.API.info(row.fieldByName('nbeds'));
    return row.fieldByName('nbeds');
};


module.exports.camaOcupada = function(nivel,cama){
	var db = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM patients WHERE room=? AND bed=?;';
	var rows = db.execute(sql,nivel,cama);
	// Bed Taken
	if(rows.isValidRow()) {
  		db.close();
		return 1;
  	}
  	// Bed Not Taken
  	else{
  		db.close();
		return 0;  		
  	}
};

module.exports.camaValida = function(nivel,cama){
	var mydb = Ti.Database.open(DATABASE_NAME);
	var sql = 'SELECT * FROM rooms;';
	var row = mydb.execute(sql);
	// Room is within boundaries
    if(row.rowCount<nivel && nivel > 0) {
    	mydb.close();
		return 0;
  	}
  	else{
		lvl = nivel-1;
  		sql = 'SELECT beds FROM rooms LIMIT ? OFFSET ?';
  		row = mydb.execute(sql, nivel, lvl);
  		if(row.fieldByName('beds') < cama){
  			mydb.close();
			return 0;
  		}
  		else{
  			mydb.close();
			return 1;
  		}
  	}
};

function getDate(currentTime) {
	try{
		var month = pad(currentTime.getMonth() + 1);
		var day = pad(currentTime.getDate());
		var year = currentTime.getFullYear();
		return year+'-'+month+'-'+day;	
	}
	catch(e){
		return currentTime;
	}
	
}

function pad(n) {
    return (n < 10) ? ("0" + n) : n;
}