function AboutView () {
	
	var RoomsView = require('ui/common/RoomsView');
	
	var about = Ti.UI.createWindow({
		backgroundColor: '#2e7ab8',
		barColor: '#4893d1',
		translucent : true,
		navBarHidden: false,
		title: L('settings'),
	});
	
	var win1 = Titanium.UI.iOS.createNavigationWindow({
		tintColor: '#FFFFFF',
    	window : about
	});
	
	var roomsView = new RoomsView();
	roomsView.refreshRoomList();
	// Stuff to add new rooms and beds
	
	roomsView.addEventListener('roomsChanged', function() {
		win1.fireEvent('roomsChanged');	
	});
	
	
	// About information
	 
	var view2 = Ti.UI.createView({
	    backgroundColor:'#2e7ab8'
	});
	
	var info = Ti.UI.createLabel({
	    text:'\tYou are using Patients Notes (This is a work in progress).\n\n\t Intended for offline use but lots of improvements are planned.\n\n\tAny sugestions please email them to: \nvascoramos@fluiddo.com.',
	    height:'auto',
	    width:'90%',
	    top:'10%',
	    font: {
	        fontSize:20 
	    },
	    color:'#FFFFFF',
	});
	 


	var scrollable = Ti.UI.createScrollableView({
		showPagingControl:true,
	    pagingControlColor:'#2e7ab8',
	    views:[roomsView, view2],
	    showPagingControl:true,
		pagingControlHeight:30,
		maxZoomScale:2.0,
		currentPage:2,
	});
	 
	view2.add(info);
	about.add(scrollable);
	 

	// Close the window
	// Review this later ffs
	var cancelButton = Ti.UI.createButton({
		style: Titanium.UI.iPhone.SystemButton.INFO_LIGHT,
		title: 'Cancel',
		color: '#FFFFFF',
	});
	
	cancelButton.addEventListener('click', function(e){
		win1.close();
	});
	
	 
	about.leftNavButton=cancelButton;
	
	return win1;		  
}

module.exports = AboutView;