function printDateAndTime(date){
	var day=date.slice(8,10);
	var month=date.slice(5,7);
	var year=date.slice(0,4);
	var hours=date.slice(11,13);
	var minutes=date.slice(14,16);
	return L('note_created_on')+' '+day+'/'+month+'/'+year+' '+L('at')+' '+hours+':'+minutes;
}

var createNoteRow = function(nota) {
	var tablerow = Ti.UI.createTableViewRow({
		hasChild: false,
		selectedBackgroundColor:'#81B5DF',
		backgroundColor:'#5098D3',
		editable:false,
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
	});
	
	var viewParent = Ti.UI.createView({
    	bottom: 10, // use as margin from the row edges
        left: 10, // use as margin from the row edges
        right: 10, // use as margin from the row edges
        top: 10, // use as margin from the row edges
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,    
    });
	
	tablerow.add(viewParent);
	
	var date = Ti.UI.createLabel({
		top:0,
		right:0,
		width:Ti.UI.SIZE,
		height:20,
		font:{fontSize:15,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		text: printDateAndTime(nota.createdDate),
		right: 5,
	});
	
	var notaLabel = Ti.UI.createLabel({	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		top:30,
        left: 0,
        text: (nota.text || ''),
        width: Ti.UI.SIZE,
		color:'#FFFFFF',
	});
	
	var spine = Ti.UI.createView({
		width:Ti.UI.SIZE,
		height:180,
		top: 150,
		bottom:10,
	});
	var spine1 = Ti.UI.createImageView({
		backgroundImage: 'images/spine1.png',
		width:'45%',
		top:0,
		height: 60,
		left:0,
	});
	
	var spine2 = Ti.UI.createImageView({
		backgroundImage: 'images/spine2.png',
		width:'120',
		bottom:0,
		height:60,
	});
	
	var spine3 = Ti.UI.createImageView({
		backgroundImage: 'images/spine3.png',
		width:'45%',
		top:0,
		height:60,
		right:0,
	});

	spine.add(spine1);
	spine.add(spine2);
	spine.add(spine3);
	
	
	var na = Ti.UI.createLabel({
		text: nota.na,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '2%',
		top: '5%',
	});
	
	spine.add(na);
	
	var k = Ti.UI.createLabel({
		text: nota.k,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '2%',
		top: '30%',
	});
	
	spine.add(k);
	
	var cl = Ti.UI.createLabel({
		text: nota.cl,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '13%',
		top: '5%',
	});
	
	spine.add(cl);
	
	var urea = Ti.UI.createLabel({
		text: nota.urea,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '13%',
		top: '30%',
	});
	
	spine.add(urea);
	
	var crea = Ti.UI.createLabel({
		text: nota.crea,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '26%',
		top: '5%',
	});
	
	spine.add(crea);
	
	var lact = Ti.UI.createLabel({
		text: nota.lact,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '26%',
		top: '30%',
	});
	
	spine.add(lact);
	
	var hgb = Ti.UI.createLabel({
		text: nota.hgb,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '26%',
		top: '5%',
	});
	
	spine.add(hgb);
	
	var ht = Ti.UI.createLabel({
		text: nota.ht,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '26%',
		top: '30%',
	});
	
	spine.add(ht);
	
	var alt = Ti.UI.createLabel({
		text: nota.alt,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '13%',
		top: '5%',
	});
	
	spine.add(alt);
	
	var ast = Ti.UI.createLabel({
		text: nota.ast,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '13%',
		top: '30%',
	});
	
	spine.add(ast);
	
	var plaq = Ti.UI.createLabel({
		text: nota.plaq,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '0%',
		top: '5%',
	});
	
	spine.add(plaq);
	
	
	var leuco = Ti.UI.createLabel({
		text: nota.leuco,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '45%',
		bottom: '26%',
	});
	
	spine.add(leuco);
	
	var pcr = Ti.UI.createLabel({
		text: nota.pcr,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '45%',
		bottom: '6%',
	});
	
	spine.add(pcr);
	
	var vanco = Ti.UI.createLabel({
		text: nota.vanc,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		left: '30%',
		bottom: '16%',
	});
	
	spine.add(vanco);
	
	var valp = Ti.UI.createLabel({
		text: nota.valp,
		textAlign:'left',
		color: 'white',
		height: '10%',
		font: {
			fontSize: 16,
		},
		width: '10%',
		right: '25%',
		bottom: '16%',
	});
	
	spine.add(valp);
	
	
	
	if(nota.na||nota.k||nota.cl||nota.urea||nota.crea||nota.lact||
		nota.leuco||nota.hgb||nota.ht||nota.alt||nota.ast||nota.plaq||
		nota.vanco||nota.valp||nota.pcr){
		viewParent.add(spine);	
	}
	
	viewParent.add(date);
	viewParent.add(notaLabel);
					
	return tablerow;
};

var createInfoDelRow = function() {
	var tablerow = Ti.UI.createTableViewRow({
		hasChild: false,
		selectedBackgroundColor:'#81B5DF',
		backgroundColor:'#5098D3',
		editable:false,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
	});
	
	var viewParent = Ti.UI.createView({
    	bottom: 10, // use as margin from the row edges
        left: 10, // use as margin from the row edges
        right: 10, // use as margin from the row edges
        top: 10, // use as margin from the row edges
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,    
    });
    
	
	var infoLabel = Ti.UI.createLabel({
		width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: '#fff',
        text: L('deleted_patient'),
	});

	
	tablerow.add(viewParent);
	viewParent.add(infoLabel);
	return tablerow;
};

var createFreshRow = function(i) {
	var tablerow = Ti.UI.createTableViewRow({
		hasChild: false,
		selectedBackgroundColor:'#81B5DF',
		backgroundColor:'#5098D3',
		editable:false,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
	});
	
	var viewParent = Ti.UI.createView({
    	bottom: 10, // use as margin from the row edges
        left: 10, // use as margin from the row edges
        right: 10, // use as margin from the row edges
        top: 10, // use as margin from the row edges
        width: '90%',
        height: Ti.UI.SIZE,    
    });
    
    var imageView = Titanium.UI.createImageView({
		image:'/images/settings.png',
		width:48,
		height:48,
		right:0,
	});
    
    if(i==1){
    	imageView.setImage('/images/settings.png');
    }
    else if(i==2){
    	imageView.setImage('/images/addpatient.png');
    }
    	
	var infoLabel = Ti.UI.createLabel({
		width: '80%',
        height: Ti.UI.SIZE,
        left:0,
        color: '#fff',
        text: L('fresh_start_'+i),
	});

	
	tablerow.add(viewParent);
	viewParent.add(infoLabel);
	viewParent.add(imageView);
	return tablerow;
};

var createEmptyNotesRow = function() {
	var tablerow = Ti.UI.createTableViewRow({
		hasChild: false,
		selectedBackgroundColor:'#81B5DF',
		backgroundColor:'#5098D3',
		editable:false,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
	});
	
	var viewParent = Ti.UI.createView({
    	bottom: 10, // use as margin from the row edges
        left: 10, // use as margin from the row edges
        right: 10, // use as margin from the row edges
        top: 10, // use as margin from the row edges
        width: '90%',
        height: Ti.UI.SIZE,    
    });
    
    var imageView = Titanium.UI.createImageView({
		image:'/images/addnote.png',
		width:48,
		height:48,
		right:0,
	});
     	
	var infoLabel = Ti.UI.createLabel({
		width: '80%',
        height: Ti.UI.SIZE,
        left:0,
        color: '#fff',
        text: L('empty_notes'),
	});

	
	tablerow.add(viewParent);
	viewParent.add(infoLabel);
	viewParent.add(imageView);
	return tablerow;
};
