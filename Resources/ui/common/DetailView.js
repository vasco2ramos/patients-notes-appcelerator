Ti.include('/ui/common/DetailRowsContructor.js');

//Detail View Component Constructor
function DetailView() {
	var self = Ti.UI.createWindow({
		backgroundColor:'#fff',
		title:L('patient'),
		//navBarHidden:true,
	});
	/*
	var PatientsInfoView = require('ui/common/PatientInfoView');	
	var patientInfo = new PatientsInfoView();			
	
	self.add(patientInfo);
		*/
	
	var notesView = Ti.UI.createView({
		top:0,
		right:0,
		
	});
	
	var table = Ti.UI.createTableView({
		style:Titanium.UI.iPhone.TableViewStyle.GROUPED,
		width:Ti.UI.FILL,
		height:Ti.UI.FILL,
	});
	
	self.add(table);
	
	table.addEventListener('click', function(e) {
		table.selectRow(e.index);
		self.fireEvent('itemSelected', { data:e.row.data, index:e.index});	
	});
	
	
	self.doTop = function(top) {
		table.setTop(top);
	};
	
	self.showPatientInfo = function(id) {
		var Database = require('services/fio');
		var notes = Database.getNotesById(id); 
		table.setData([]);
		var rows = [];
		for(var i = 0; i < notes.length; i++){
			rows.unshift(createNoteRow(notes[i]));
		}
		if(rows.length==0){
			rows.unshift(createEmptyNotesRow());
			table.setData(rows);
		}
		else{	
			table.setData(rows);
		}
	};
	
	self.clean = function(){
		var rows = [];
		// 1 means patient was deleted
		rows.unshift(createInfoDelRow());
		table.setData(rows);
	};
	
	self.freshStart = function(){
		var rows = [];
		rows.unshift(createFreshRow(1));
		rows.unshift(createFreshRow(2));
		table.setData(rows);
	}; 
	
	// Show Patient Menu
	return self;	
}
module.exports = DetailView;
