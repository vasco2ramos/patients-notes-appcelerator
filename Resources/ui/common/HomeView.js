//Home View Component Constructor
function HomeView() {
	var self = Ti.UI.createWindow({
		title:L('home'),
		backgroundColor: '#0199A6',
		tintColor: '#0199A6',
	});
	
	var doctorView = Ti.UI.createView({
		width: '90%',
		height: 200,
		top: '5%',
		left: '5%',
		backgroundColor: '#5098D3',
	}); 			
	self.add(doctorView);
	
	var patientStatistics = Ti.UI.createView({
		width: '90%',
	});
	//self.add(patientStatistics);
	
	// Rooms Button
	var roomStatistics = Ti.UI.createView({
		width: '90%',
	});
	//self.add(roomStatistics);
	
	return self;	
}
module.exports = HomeView;
