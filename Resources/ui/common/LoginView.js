function checkEmail(email) {
    var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!filter.test(email)) {
    	return false;
 	}
 	return true;
}

function LoginView() {
	var auth = require('services/authentication');
	var self = Ti.UI.createWindow({
		backgroundColor: '#2e7ab8',
		title: L('login'),
		barColor: '#4893d1',
		translucent : true,
		navBarHidden: false,
	});
	var win1 = Titanium.UI.iOS.createNavigationWindow({
		tintColor: '#FFFFFF',
    	window : self
	});
	  
	var email = Titanium.UI.createTextField({  
	    color:'#336699',  
	    top:10,  
	    width:300,  
	    height:40,  
	    hintText:'Email',  
	    keyboardType:Titanium.UI.KEYBOARD_DEFAULT,  
	    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,  
	    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    autocorrect: false,
	    autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	    keyboardType:Titanium.UI.KEYBOARD_EMAIL, 
	});  
	self.add(email);  
	var password = Titanium.UI.createTextField({  
	    color:'#336699',  
	    top:60,    
	    width:300,  
	    height:40,  
	    hintText:'Password',  
	    passwordMask:true,  
	    keyboardType:Titanium.UI.KEYBOARD_DEFAULT,  
	    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,  
	    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED  
	});  
	self.add(password);  
	var loginBtn = Titanium.UI.createButton({  
	    title:'Login',  
	    top:110,  
	    width:90,  
	    height:35,  
	    borderRadius:1,
	    color: '#fff',  
	    font:{fontFamily:'Arial',fontWeight:'bold',fontSize:14}  
	});  
	self.add(loginBtn);
	  
    loginBtn.addEventListener('click',function(e)  {
        auth.authUser(email.value,password.value);      
    }); 
    
    var cancelButton = Ti.UI.createButton({
		style: Titanium.UI.iPhone.SystemButton.INFO_LIGHT,
		title: 'Cancel',
		color: '#FFFFFF',
	});
	
	cancelButton.addEventListener('click', function(e){
		win1.close();
	});
	
	 
	self.leftNavButton=cancelButton;
		
	return win1;	
}
module.exports = LoginView;