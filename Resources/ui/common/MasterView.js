Ti.include('/ui/common/PatientsRowsConstructor.js');

//Master View Component Constructor
function MasterView() {
	var self = Ti.UI.createWindow({
		backgroundColor:'#545454',
		title: L('my_beds'),	
	});
	
	var table = Titanium.UI.createTableView({
		style:Titanium.UI.iPhone.TableViewStyle.PLAIN,
		backgroundColor:'transparent',
		separatorStyle: Ti.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		allowSelection: true,
		backgroundSelectedColor:'black',
	});
	
	self.add(table);

	table.addEventListener('click', function(e) {
		if(e.row.data){
			table.selectRow(e.index);
			self.fireEvent('patientSelected', { data:e.row.data, index:e.index});
		}	
	});
	
	var rows = [];
	self.createPatientsTable = function(rooms) {
		createFirstTable(rows, table, rooms);
	};
		
	self.refreshPatientList = function(rooms, sortBy) {
		// Add Sort By attribute here
		var Database = require('services/fio');
		var patients = Database.getPatients();
		rows = [];
		createFirstTable(rows, table,rooms);
		if (Object.prototype.toString.apply(patients) === '[object Array]') {
			var i;
			for (i = 0; i < patients.length; i++) {
				var nivel = patients[i].room;
				var addto = nivel-1;
				var camas = rows[addto].rows;
				camas[patients[i].bed-1]=createPatientRow(patients[i]);
				rows[addto].rows=camas;
			}		
			table.setData(rows);
		}
	};
	
	return self;	
}
module.exports = MasterView;