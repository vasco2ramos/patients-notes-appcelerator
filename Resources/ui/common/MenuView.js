function MenuView(patient){
	var menu = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: 80,
		width:'100%',
		bottom: 0,
		left: 0,
    });
    
    var box = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		top:5,
		bottom:5,
		left:5,
		height:70,
		width: 200,
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#7591A8'
    });
    
    var info = Titanium.UI.createLabel({
		font:{fontSize:16,fontFamily:'Trebuchet MS'},
		height:70,
		width: Ti.UI.SIZE,
		color: 'white',
		text: L('room')+' - '+patient.room+"   "+L('bed')+' - '+patient.bed+'\n'+patient.name
	});
	box.add(info);
	
	var changePatientView = Ti.UI.createView({
		left: 250,
		top:5,
		width: 140,
		height:70,
    });
	
  	var alterarDoente = Ti.UI.createButton({
		title: L('change_patient'),
		color: '#fff',
		width: 140,
		height:20,
		left:0,
		top:0,
		font:{fontSize:15,fontFamily:'Trebuchet MS'},
	});
	
	var alterarDoenteImage = Titanium.UI.createImageView({
		image:'/images/editnote.png',
		width:48,
		height:48,
		top:21
	});
	changePatientView.add(alterarDoenteImage);
	changePatientView.add(alterarDoente);
	
	var changeNoteView = Ti.UI.createView({
		left: 400,
		top:5,
		width: 140,
		height:70,
    });
	
	var adicionarNota = Ti.UI.createButton({
		title: L('add_note'),
		color: '#fff',
		width: 140,
		height:20,
		top:0,
		font:{fontSize:15,fontFamily:'Trebuchet MS'},
	});
	
	var adicionarNotaImage = Titanium.UI.createImageView({
		image:'/images/addnote.png',
		width:48,
		height:48,
		top:21
	});
	changeNoteView.add(adicionarNotaImage);
	changeNoteView.add(adicionarNota);
	
	var deletePatientView = Ti.UI.createView({
		left: 550,
		top:5,
		width: 140,
		height:70,
    });
	
	var eliminarDoente = Ti.UI.createButton({
		title: L('delete_patient'),
		color: '#fff',
		width: 140,
		height:20,
		top:0,
		font:{fontSize:15,fontFamily:'Trebuchet MS'},
	});
	
	var eliminarDoenteImage = Titanium.UI.createImageView({
		image:'/images/deletepatient.png',
		width:48,
		height:48,
		top:21
	});
	deletePatientView.add(eliminarDoenteImage);
	deletePatientView.add(eliminarDoente);
	
	changePatientView.addEventListener('click', function(e) {
		box.fireEvent('changePatient', {});
	});
	changeNoteView.addEventListener('click', function(e) {
		box.fireEvent('addNote', {});
	});
	deletePatientView.addEventListener('click', function(e) {
		box.fireEvent('deletePatient', {});
	});

	menu.add(box);
	menu.add(changePatientView);
	menu.add(changeNoteView);
	menu.add(deletePatientView);
	return menu;
}
module.exports = MenuView;