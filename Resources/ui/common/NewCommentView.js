function NewCommentView (patientId,note) {
	var Database = require('services/fio');
	
	var novaNota = Ti.UI.createWindow({
		title: L('new_note'),
		backgroundColor: '#2e7ab8',
		barColor: '#4893d1',
		navBarHidden: false,
	});
	
	var win1 = Titanium.UI.iOS.createNavigationWindow({
		tintColor: '#FFFFFF',
    	window : novaNota,
	});
	
	var box = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: '85%',
		width:'90%',
		top: 60,
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0'
    });
	
	
	var notaLabel = Ti.UI.createLabel({
		top:10,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'90%',
		color: 'white',
		text: L('type_note'),
	});
	
	var nota = Ti.UI.createTextArea({
		height:200,
		top:40,
		width: '90%',
		color:'#888',
		font:{fontSize:16,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		textAlign:'left',
		suppressReturn:false,
		autocorrect: false,
		keyboardType:Titanium.UI.KEYBOARD_ASCII,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED			
	});
	
	
	var guardar = Ti.UI.createButton({
		title : L('save_note'),
		backgroundGradient:{
			type:'linear', 
			colors:['#FFFFFF','#EFEFEF'], 
			startPoint:{y:0}, 
			endPoint:{y:40}, 
			backFillStart:false
			},
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0',
		width : 200,
		height : 35,
		top:10,
		left:'5%',
		backgroundImage: 'none',
		color: '#67A73E',
	});
	
	guardar.addEventListener('click', function(e){
		// Save First Ofcourse
		win1.fireEvent('gravarInfo');
		win1.close();
	});
	
	var spine = Ti.UI.createView({
		width:'90%',
		height:150,
		top:280,
	});
	var spine1 = Ti.UI.createImageView({
		backgroundImage: 'images/spine1.png',
		width:'45%',
		top:0,
		height:60,
		left:0,
	});
	
	var spine2 = Ti.UI.createImageView({
		backgroundImage: 'images/spine2.png',
		width:'120',
		bottom:0,
		height:60,
	});
	
	var spine3 = Ti.UI.createImageView({
		backgroundImage: 'images/spine3.png',
		width:'45%',
		top:0,
		height:60,
		right:0,
	});
	spine.add(spine1);
	spine.add(spine2);
	spine.add(spine3);
	
	var na = Ti.UI.createTextField({
		height:28,
		top:0,
		left:0,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Na',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(na);
	
	var k = Ti.UI.createTextField({
		height:28,
		top:32,
		left:0,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'K',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(k);
	
	var cl = Ti.UI.createTextField({
		height:28,
		top:0,
		left:51,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Cl',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(cl);
	
	var urea = Ti.UI.createTextField({
		height:28,
		top:32,
		left:51,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Urea',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(urea);
	
	var crea = Ti.UI.createTextField({
		height:28,
		top:0,
		left:108,
		width: 40,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Creat',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(crea);
	
	
	var lact = Ti.UI.createTextField({
		height:28,
		top:32,
		left:108,
		width: 40,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Lact',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(lact);
	
	var hgb = Ti.UI.createTextField({
		height:28,
		top:0,
		right:108,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'hgb',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(hgb);
	
	var ht = Ti.UI.createTextField({
		height:28,
		top:32,
		right:108,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'HT',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(ht);
	
	var alt = Ti.UI.createTextField({
		height:28,
		top:0,
		right:51,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'ALT',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(alt);
	
	var ast = Ti.UI.createTextField({
		height:28,
		top:32,
		right:51,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'AST',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(ast);
	
	var plaq = Ti.UI.createTextField({
		height:28,
		top:0,
		right:0,
		width: 37,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Plaq',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(plaq);
	
	var leuco = Ti.UI.createTextField({
		height:28,
		top:76,
		left:178,
		width: 70,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Leucocytes',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(leuco);
	
	
	var pcr = Ti.UI.createTextField({
		height:28,
		top:130,
		left:180,
		width: 70,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'PCR',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(pcr);
	
	var vanco = Ti.UI.createTextField({
		height:28,
		top:100,
		left:128,
		width: 40,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Vanco',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(vanco);
	
	var valp = Ti.UI.createTextField({
		height:28,
		top:100,
		left:268,
		width: 40,
		font:{fontSize:14,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'white',
		hintText:'Valp',
		backgroundColor: '#2e7ab8', 
    	backgroundImage:'none',
		borderWidth:0,
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,			
	});
	spine.add(valp);
	
	
	if(note==null){}
	else{
		nota.value = note.nota;
		na.value = note.na;
		k.value = note.k;
		cl.value = note.cl;
		urea.value = note.urea;
		crea.value = note.crea;
		lact.value = note.lact;
		leuco.value = note.leuco;
		hgb.value = note.hgb;
		ht.value = note.ht;
		alt.value = note.alt;
		ast.value = note.ast;
		plaq.value = note.plaq;
		vanco.value = note.vanco;
		valp.value = note.valp;
		pcr.value = note.pcr;
	}
	
	
	guardar.addEventListener('click', function(e){
		if(nota.value){
			note = {
				text: nota.value,
				na:na.value,
				k:k.value,
				cl:cl.value,
				urea:urea.value,
				crea:crea.value,
				lact:lact.value,
				leuco:leuco.value,
				hgb:hgb.value,
				ht:ht.value,
				alt:alt.value,
				ast:ast.value,
				plaq:plaq.value,
				vanc: vanco.value,
				valp: valp.value,
				pcr: pcr.value,
			};
			Database.addNote(patientId,note);
			win1.fireEvent('refreshTable', {});
			win1.close();
		}
		else{
			alert(L('all_fields'));
		}
	});
	
	var cancelButton = Ti.UI.createButton({
		title : L('cancel'),
		backgroundGradient:{
			type:'linear', 
			colors:['#FFFFFF','#EFEFEF'], 
			startPoint:{y:0}, 
			endPoint:{y:40}, 
			backFillStart:false
			},
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0',
		width : 200,
		height : 35,
		top:10,
		right:'5%',
		backgroundImage: 'none',
		color: '#CD1625'
	});
	
	cancelButton.addEventListener('click', function(e){
		win1.close();
	});
	
	novaNota.add(cancelButton);
	novaNota.add(box);
	novaNota.add(guardar);
	box.add(spine);
	box.add(notaLabel);
	box.add(nota);
	return win1;		  
	
}
module.exports = NewCommentView;