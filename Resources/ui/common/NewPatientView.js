function makeDateFromIsoString(isoString){
	isoString = JSON.stringify(isoString);
	isoString = JSON.parse(isoString);
	if (isoString == null)
	return null;
	var date = new Date();
	date.setFullYear(isoString.substr(0, 4));
	date.setDate(isoString.substr(8, 2));
	date.setMonth(isoString.substr(5, 2) - 1);
	date.setHours(isoString.substr(11, 2));
	date.setMinutes(isoString.substr(14, 2));
	date.setSeconds(isoString.substr(17, 2));
	return date;
}

var createPicker = function(){
	var date = new Date();
	var picker = Ti.UI.createPicker({
			type:Ti.UI.PICKER_TYPE_DATE,
			value: date,
			maxDate:date
	});
	picker.setLocale(Titanium.Platform.locale);
	picker.addEventListener('change',function(e){
    });
    picker.fireEvent('change');
	return picker;
};


var createTablePicker = function(number){
	var table = Titanium.UI.createTableView({
		style:Titanium.UI.iPhone.TableViewStyle.PLAIN,
		backgroundColor:'transparent',
		separatorStyle: Ti.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		allowSelection: true,
		backgroundSelectedColor:'black',
		width: 30,
		height: 60,
	});
	var i;
	var rows =[];
	for(i=0;i<number;i++){
		var tablerow = Ti.UI.createTableViewRow({
			height: 35,
			className: 'itemRow',
			hasChild: false,
			selectedBackgroundColor: '#ffffff',
			backgroundColor:'#ffffff',
		});
		var numberLabel = Ti.UI.createLabel({
			text: i+1,
			textAlign:'center',
			color: '#000',
			font: {
				fontWeight:'bold',
				fontSize: 16,
			},
		});
		tablerow.add(numberLabel);
		rows.push(tablerow);
	}
	table.setData(rows);
	return table;
};



function NewPatientView (patientInfo, mode) {
	var Database = require('services/fio');
	var estadoBuffer = 'stable';
	var rooms = Database.getRooms();
	
	var novoPaciente = Ti.UI.createWindow({
		backgroundColor: '#2e7ab8',
		barColor: '#4893d1',
		navBarHidden: false,
		titleControl: Ti.UI.createLabel({
        	text: L('add_patient'),
        	color: '#fff',
        	font: {fontSize:18, fontWeight:'bold'}
   		})
	});
	
	var win1 = Titanium.UI.iOS.createNavigationWindow({
		tintColor: '#FFFFFF',
    	window : novoPaciente,
	});
	
	var box = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: '85%',
		width:'90%',
		top: 60,
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0'
    });

	var nomeLabel = Ti.UI.createLabel({
		top:10,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'70%',
		left: '5%',
		color: 'white',
		text: L('name'),
	});
	
	var nome = Ti.UI.createTextField({
		height:30,
		top:35,
		width: '70%',
		font:{fontSize:20,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'#888',
		left: '5%',
		textAlign:'left',
		hintText:L('name'),
		keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		autocorrect: false,			
	});
	
	
	
	var genderLabel = Ti.UI.createLabel({
		top:10,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'15%',
		color: 'white',
		right: '5%',
		text: L('gender'),
	});
	var gender = Titanium.UI.iOS.createTabbedBar({
		labels:[L('gender_m'), L('gender_f')],
		backgroundColor:'#ffffff',
		top:35,
		style:Titanium.UI.iPhone.SystemButtonStyle.BAR,
		height:30,
		width:'15%',
		right: '5%',
	});
	
	
	var motivoAdmissaoLabel = Ti.UI.createLabel({
		top:75,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'90%',
		color: 'white',
		text: L('reason'),
	});
	
	var motivoAdmissao = Titanium.UI.createTextField({
		height:30,
		width:'90%',
		top:100,
		font:{fontSize:20,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'#888',
		textAlign:'left',
		hintText: L('reason'),
		keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		autocorrect: false,			
	});
	
	var salaLabel = Titanium.UI.createLabel({
		top:140,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'20%',
		left: '5%',
		color: 'white',
		text: L('room'),	
	});
	/*	 
	var roomButton =  Titanium.UI.createButton({
		backgroundImage: 'images/downarrow.png',
		width:26,
		height:26,
	});
	
	roomPickerOpen=0;
	roomButton.addEventListener('click', function(e){
		if(roomPickerOpen==0){
			roomPickerOpen = 1;
			var nrooms = Database.getNumberOfRooms();
			var tablePicker = createTablePicker(nrooms);
			tablePicker.setTop(140);
			tablePicker.setLeft('25%');
			box.add(tablePicker);
			tablePicker.addEventListener('click', function(e) {
				sala.value = e.index+1;
				box.remove(tablePicker);
				roomPickerOpen = 0;	
			});	
		}	
	});
	*/
	var sala = Titanium.UI.createTextField({
		height:30,
		width:'20%',
		left:'5%',
		top:165,
		font:{fontSize:20,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'#888',
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		//rightButton:roomButton,
		//rightButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS,
		autocorrect: false,	
	});
	
	//sala.addEventListener('focus', function(e){ sala.blur() ;});
	
	var camaLabel = Titanium.UI.createLabel({
		top:140,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'20%',
		left: '30%',
		color: 'white',
		text: L('bed'),
	});
	/*
	var bedButton =  Titanium.UI.createButton({
		backgroundImage: 'images/downarrow.png',
		width:26,
		height:26,
	});
	
	 bedButton.addEventListener('click', function(e){
		if(sala.value){
			var nbeds = Database.getNumberOfBeds(sala.value);
			var tablePicker = createTablePicker(nbeds);
			tablePicker.setTop(160);
			tablePicker.setLeft('30%');
			tablePicker.setWidth('20%');
			box.add(tablePicker);
			tablePicker.addEventListener('click', function(e) {
				sala.value = e.index+1;
				box.remove(tablePicker);	
			});
		}	
	});
	*/
	
	var cama = Titanium.UI.createTextField({
		height:30,
		width:'20%',
		left:'30%',
		top:165,
		font:{fontSize:20,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'#888',
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		//rightButton:bedButton,
		//rightButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS,
		autocorrect: false,		
	});
	
	//cama.addEventListener('focus', function(){ cama.blur() ;});
	
	var estadoLabel = Titanium.UI.createLabel({
		top:140,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'20%',
		left: '55%',
		color: 'white',
		text: L('state'),	
	});
	
	var estadoDisplay = Titanium.UI.createLabel({
		top:165,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:30,
		width:'20%',
		left: '55%',
		color: 'green',
		text: L('stable'),
	});
	
	var estadoAlterar = Ti.UI.createButton({
		title: L('change'),
		width:70,
		height:30,
		left:'80%',
		top:165,
		font:{fontSize:15,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0',
		backgroundImage: 'none',
		color: '#FFFFFF'
	});
	
	var dataNascimento = Ti.UI.createLabel({
		text: L('birth_date'),
		value: '',
		width:'40%',
		left: '5%',
		height:20,
		top:205,
		color:'white',
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'}
	});
	var dataNascimentoAlterar = Ti.UI.createButton({
		width:'90%',
		height:90,
		left:'5%',
		top:230,
		backgroundImage: 'none',
		color: '#FFFFFF'
	});
	var picker1 = createPicker();
	dataNascimentoAlterar.add(picker1);
	
	
	var dataAdmissao = Ti.UI.createLabel({
		text: L('admission_date'),
		value: '',
		width:'40%',
		left: '5%',
		color:'white',
		height:20,
		top:330,
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'}
	});
	var dataAdmissaoAlterar = Ti.UI.createView({
		width:'90%',
		height:90,
		left:'5%',
		top:355,
		backgroundImage: 'none',
		color: '#FFFFFF'
	});
	var picker2 = createPicker();
	dataAdmissaoAlterar.add(picker2);
	

	
	estadoAlterar.addEventListener('click', function(e){
		if(estadoBuffer == 'stable'){
			estadoBuffer = 'req_atention';
			estadoDisplay.text= L('req_atention');
			estadoDisplay.color='yellow';
		}
		else if(estadoBuffer == 'req_atention'){
			estadoBuffer = 'critical';
			estadoDisplay.text=L('critical');
			estadoDisplay.color='red';
		}
		else{
			estadoBuffer = 'stable';
			estadoDisplay.text= L('stable');
			estadoDisplay.color='green';
		}
	});
	
		
	var saveButton = Ti.UI.createButton({
		title : L('save_patient'),
		backgroundGradient:{
			type:'linear', 
			colors:['#FFFFFF','#EFEFEF'], 
			startPoint:{y:0}, 
			endPoint:{y:40}, 
			},
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0',
		width : 200,
		height : 35,
		top:10,
		left:'5%',
		backgroundImage: 'none',
		color: '#67A73E'
	});
	
	var cancelButton = Ti.UI.createButton({
		title : L('cancel'),
		backgroundGradient:{
			type:'linear', 
			colors:['#FFFFFF','#EFEFEF'], 
			startPoint:{y:0}, 
			endPoint:{y:40}, 
			},
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0',
		width : 200,
		height : 35,
		top:10,
		right:'5%',
		backgroundImage: 'none',
		color: '#CD1625'
	});
	
	cancelButton.addEventListener('click', function(e){
		win1.close();
	});
	
	saveButton.addEventListener('click', function(e){
		dataNascimento.value = picker1.value;
		dataAdmissao.value = picker2.value;
		if(nome.value=='' || sala.value==''|| cama.value=='' || gender.index==null){
			alert(L('all_fields'));
			return;
		}
		else if(sala.value != parseInt(sala.value) && cama.value != parseInt(cama.value)){
			alert(L('insert_int_warning'));
			return;
		}
		else if(cama.value!=patientInfo.bed || sala.value!=patientInfo.room){
			if(Database.camaOcupada(sala.value,cama.value)){
				var msg = L('bed')+' '+cama.value+' '+L('from_room')+' '+sala.value+' '+L('is_taken');
				alert(msg);
				return;
			}
			else if(!Database.camaValida(sala.value,cama.value)){
				var msg = L('bed')+' '+cama.value+' '+L('from_room')+' '+sala.value+' '+L('is_invalid');
				alert(msg);
				return;
			}
		}
		var d = new Date();
		// It's a new patient
		if(mode==1){
			var patient = {
				name: nome.value,
				birthdate: dataNascimento.value,
				admissiondate: dataAdmissao.value,
				reason: motivoAdmissao.value,
				room: sala.value,
				bed: cama.value,
				createddate: d.toLocaleDateString(),
				modifieddate: d.toLocaleDateString(),
				state: estadoBuffer,
				sex: gender.index,
			};	
			Database.addPatient(patient);	
		}
		else{
			var patient = {
				id: patientInfo.id,
				name: nome.value,
				birthdate: dataNascimento.value,
				admissiondate: dataAdmissao.value,
				reason: motivoAdmissao.value,
				room: sala.value,
				bed: cama.value,
				createddate: patientInfo.createddate,
				modifieddate: d.toLocaleDateString(),
				state: estadoBuffer,
				sex: gender.index,
			};
			Database.updatePatient(patient);	
		}
		win1.fireEvent('refreshTable', {});
		win1.close();
	});

	if(mode!=1){
		nome.value = patientInfo.name;
		picker1.setValue(makeDateFromIsoString(patientInfo.birthdate));
		picker2.setValue(makeDateFromIsoString(patientInfo.admissiondate));
		motivoAdmissao.value = patientInfo.reason;
		cama.value = patientInfo.bed;
		sala.value = patientInfo.room;
		estadoBuffer =  patientInfo.state;
		gender.index = patientInfo.sex;
		if(estadoBuffer == 'stable'){
			estadoBuffer = 'stable';
			estadoDisplay.text= L('stable');
			estadoDisplay.color='green';
		}
		else if(estadoBuffer == 'req_atention'){
			estadoBuffer = 'req_atention';
			estadoDisplay.text= L('req_atention');
			estadoDisplay.color='yellow';
		}
		else{
			estadoBuffer = 'critical';
			estadoDisplay.text=L('critical');
			estadoDisplay.color='red';
		}
	}
	
	
	box.add(nomeLabel);
	box.add(nome);
	box.add(genderLabel);
	box.add(gender);
	box.add(motivoAdmissaoLabel);
	box.add(motivoAdmissao);
	box.add(camaLabel);
	box.add(cama);
	box.add(salaLabel);
	box.add(sala);
	box.add(estadoLabel);
	box.add(estadoDisplay);
	box.add(estadoAlterar);
	box.add(dataNascimento);
	box.add(dataNascimentoAlterar);
	box.add(dataAdmissao);
	box.add(dataAdmissaoAlterar);
	
	novoPaciente.add(box);
	novoPaciente.add(saveButton);
	novoPaciente.add(cancelButton);
	
	
	return win1;				  
}

module.exports = NewPatientView;

