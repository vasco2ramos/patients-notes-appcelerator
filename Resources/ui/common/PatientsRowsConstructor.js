var createFirstTable = function(rows, table, rooms){
	var size = rooms.length;
	for (var r = 0; r < size; r++) {			
		var section = Ti.UI.createTableViewSection();
		var customView = Ti.UI.createView({height:'auto',backgroundColor:'#0199A6',});
		var customLabel = Ti.UI.createLabel({
		    top:5, bottom:5,
		    height:'auto',
		    textAlign:'center',
		    font:{fontSize:14, fontWeight:'bold'},
		    color:'white',
		});
		customView.add(customLabel);
		section.headerView = customView;
		// FIXME : Sort By feature requires this to be dynamic 
		customLabel.text=L('room')+' '+(r+1);
		rows.push(section);
		for(var b = 0; b<rooms[r]; b++){
			section.add(createBedRow(r+1,b+1));
		}
	}
	table.setData(rows);
};


var createBedRow = function(room, bed){
	var tablerow = Ti.UI.createTableViewRow({
		height: 35,
		className: 'itemRow',
		hasChild: false,
		selectedBackgroundColor: '#545454',
		backgroundColor:'#545454',
	});
	
	var bedview = Ti.UI.createLabel({
		text: room+' - '+L('bed')+' '+ bed,
		textAlign:'center',
		color: '#000',
		borderColor: 'white',
		borderRadius: 5,
		borderWidth: 2,
		height: 25,
		font: {
			fontWeight:'bold',
			fontSize: 16,
		},
		width: '40%',
		left: 5,
		top: 5,
	});
	
	var free = Ti.UI.createLabel({
		text: L('free'),
		textAlign:'center',
		color: 'gray',
		height: 25,
		font: {
			fontWeight:'bold',
			fontSize: 16,
		},
		width: '40%',
		right: 5,
		top: 5,
	});
	
	tablerow.add(bedview);
	tablerow.add(free);
	return tablerow;
};

var createPatientRow = function(item) {
	var tablerow = Ti.UI.createTableViewRow({
		height: 130,
		link: item.link,
		className: 'itemRow',
		data: item,
		hasChild: true,
		selectedBackgroundColor: '#545454',
		backgroundColor:'#545454',
	});
	
	// FIXME : Improve this calculations
	var today = new Date();
	var admissiondate = item.admissiondate;
	adm = new Date(admissiondate.slice(0,4),admissiondate.slice(5,7)-1,admissiondate.slice(8,10));
	diff = today -adm;
	diffdays = diff / 1000 / (60 * 60 * 24);
	var los = Math.floor(diffdays);
	
	var losview = Ti.UI.createLabel({
		text: los+' '+L('length_stay'),
		color: '#C2C2C2',
		width: 150,
		height: 15,
		textAlign:'left',
		font: {
			fontSize: 12,
		},
		left:25,
		top:5,
	});
	
	var nameview = Ti.UI.createLabel({
		text: item.name,
		color: '#C2C2C2',
		width: 130,
		height: 15,
		textAlign:'right',
		font: {
			fontSize: 12,
		},
		right: 5,
		top: 5,
	});
	
	var estadoview = Ti.UI.createImageView({
		right: 5,
		bottom: 5,
		width: 25,
		height: 25,
	});
	
	if(item.state=='stable'){
		estadoview.setImage('images/green.png');
	}
	else if(item.state=='req_atention'){
		estadoview.setImage('images/yellow.png');
	}
	else{
		estadoview.setImage('images/red.png');
	}
	
	var roomLabel = Ti.UI.createLabel({
		text: L('room').toUpperCase(),
		textAlign:'left',
		color: '#0199A6',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 50,
		left: 180,
		top: 35,
	});
	tablerow.add(roomLabel);
	
	var bedLabel = Ti.UI.createLabel({
		text: L('bed').toUpperCase(),
		textAlign:'left',
		color: '#0199A6',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 50,
		left: 180,
		top: 55,
	});
	tablerow.add(bedLabel);
	
	var bedNumber = Ti.UI.createLabel({
		text: item.bed,
		color: '#fff',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 50,
		left: 230,
		top: 55,
	});
	tablerow.add(bedNumber);
	
	var genderLabel = Ti.UI.createLabel({
		text: L('gender').toUpperCase(),
		textAlign:'left',
		color: '#0199A6',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 100,
		left: 25,
		top: 35,
	});
	tablerow.add(genderLabel);
	
	var genderInfo = Ti.UI.createLabel({
		color: '#fff',
		textAlign:'left',
		height: 15,
		font: {
			fontSize: 12,
		},
		left: 110,
		top: 35,
	});
	tablerow.add(genderInfo);
	
	var ageLabel = Ti.UI.createLabel({
		text: L('age').toUpperCase(),
		textAlign:'left',
		color: '#0199A6',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 150,
		left: 25,
		top: 55,
	});
	tablerow.add(ageLabel);
	
	//Calculating age
	// FIXME :  Improve this calculations
	var birthdate = item.birthdate;
	//birthdate = birthdate.slice(0,birthdate.indexOf(' '));
	var adm = new Date(birthdate.slice(0,4),birthdate.slice(5,7)-1,birthdate.slice(8,10));
	var diff = today-adm;
	var diffdays = diff / (31557600000);
	var age = Math.floor(diffdays);
		
	var ageInfo = Ti.UI.createLabel({
		text: age+' '+L('years'),
		textAlign:'left',
		color: '#fff',
		height: 15,
		font: {
			fontSize: 12,
		},
		left: 110,
		top: 55,
	});
	tablerow.add(ageInfo);
	
	var reasonLabel = Ti.UI.createLabel({
		text: L('reason').toUpperCase(),
		textAlign:'left',
		color: '#0199A6',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 150,
		left: 25,
		top: 75,
	});
	tablerow.add(reasonLabel);
		
	
	var roomNumber = Ti.UI.createLabel({
		text: item.room,
		color: '#fff',
		height: 15,
		font: {
			fontSize: 12,
		},
		width: 50,
		left: 230,
		top: 35,
	});
	tablerow.add(roomNumber);
	
	var reasonInfo = Ti.UI.createLabel({
		text: item.reason,
		textAlign:'left',
		color: '#fff',
		height: 40,
		font: {
			fontSize: 12,
		},
		left: 25,
		top: 95,
	});


	/*

	
	tablerow.add(genderview);
	tablerow.add(ageview);
	tablerow.add(bedview);
	
	tablerow.add(motivoview);
	
	* */
	
	if(item.sex==0){
		genderInfo.setText(L('gender_m'));
		genderInfo.setColor('#007FFF');
	}
	else{
		genderInfo.setText(L('gender_f'));
		genderInfo.setColor('#E30B5C');
	}
	
	tablerow.add(estadoview);
	tablerow.add(nameview);
	tablerow.add(losview);
	return tablerow;
};