/**
 * @author Vasco Ramos
 */

function PatientsSplitView() {	
	var PatientsView = require('ui/common/PatientsView'),
		DetailView = require('ui/common/DetailView');
		
	var patientsView = new PatientsView();
	var detailView = new DetailView();	
		
	var self = Ti.UI.iPad.createSplitWindow({
    	detailView:detailView,
    	masterView:patientsView,
	});
	
	patientsView.addEventListener('patientSelected', function(e) {
		self.setMasterPopupVisible(false);
		Ti.API.log('Adding Menu View');
		//detailMenuView = new MenuView(e.data);
		//detailView.add(detailMenuView);
		//activePatient=e.data.id;
		detailView.showPatientInfo(e.data.id);	
	});
	
	
	self.addEventListener('visible', function(e) {
		if(e.view == 'detail') {
			e.button.title = L('my_beds');
			detailView.leftNavButton = e.button;
			Ti.API.log('Set button');
		} else if(e.view == 'master') {
			detailView.leftNavButton = null;
			Ti.API.log('Removed button');
		}	
	});
	
	return self;
}
module.exports = PatientsSplitView;	
