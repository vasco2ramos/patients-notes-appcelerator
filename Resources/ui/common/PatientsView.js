Ti.include('/ui/common/PatientsRowsConstructor.js');

//Master View Component Constructor
function PatientsView() {
	
	var self = Ti.UI.createWindow({
		backgroundColor:'#545454',
		tintColor: '#0199A6',
		title: L('my_beds'),
		//navBarHidden:true,	
	});
	
	
	var headerView = Titanium.UI.createView({
        height: 106
    });
 
    var newPatientButton = Ti.UI.createButton({
        left: 5,
        top: 48,
        image: '/images/newpatient.png',
        width: 48,
        height: 48,
    });
 	headerView.add(newPatientButton);
 	
	var search = Titanium.UI.createSearchBar({
		backgroundColor:'#545454',
		barColor:'#545454',
		showCancel:false,
		top:0,
		autocorrect:false,
	});
	headerView.add(search);
    
    
	search.addEventListener('change', function(e){
		e.value; // search string as user types
	});
	search.addEventListener('return', function(e){
		search.blur();
	});
	search.addEventListener('cancel', function(e){
		search.blur();
	});
	
	var table = Titanium.UI.createTableView({
		headerView: headerView,
		style:Titanium.UI.iPhone.TableViewStyle.PLAIN,
		backgroundColor:'transparent',
		separatorStyle: Ti.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		separatorColor: '#0199A6', 
		allowSelection: true,
		backgroundSelectedColor:'black',
		search: search,
		filterAttribute: 'data',
	});
	
	self.add(table);
	
	table.addEventListener('click', function(e) {
		if(e.row.data){
			table.selectRow(e.index);
			self.fireEvent('patientSelected', { data:e.row.data, index:e.index});
		}	
	});
	
	var rows = [];
	
		
	self.refreshPatientList = function(sortBy) {
		var Database = require('services/fio');
		// FIXME : Get these only if their values have changed!
		var patients = Database.getPatients();
		var rooms = Database.getOnlyRooms();
		rows = [];
		createFirstTable(rows, table,rooms);
		if (Object.prototype.toString.apply(patients) === '[object Array]') {
			var i;
			for (i = 0; i < patients.length; i++) {
				var nivel = patients[i].room;
				var addto = nivel-1;
				var camas = rows[addto].rows;
				camas[patients[i].bed-1]=createPatientRow(patients[i]);
				rows[addto].rows=camas;
			}		
			table.setData(rows);
		}
	};
	
	self.addEventListener('visible', function(e) {
		if(e.view == 'detail') {
			e.button.title = L('my_beds');
			detailView.leftNavButton = e.button;
			Ti.API.log('Set button');
		} else if(e.view == 'master') {
			detailView.leftNavButton = null;
			Ti.API.log('Removed button');
		}	
	});
	
	self.refreshPatientList();
	return self;	
}
module.exports = PatientsView;