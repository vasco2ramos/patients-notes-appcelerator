var createRoomRow = function(room, level){
	var tablerow = Ti.UI.createTableViewRow({
			height: 50,
			className: 'itemRow',
			hasChild: false,
			selectedBackgroundColor: '#ffffff',
			backgroundColor:'#ffffff',
			data: room.id,
		});
	
		
		var roomLabel = Ti.UI.createLabel({
			text: L('room')+' - '+ (level+1),//+ bed,
			textAlign:'center',
			color: '#000',
			height: 30,
			font: {
				fontWeight:'bold',
				fontSize: 18,
			},
			width: '40%',
			left: 5,
			top: 10,
		});
		var aux = room.beds;
		
		var beds = Ti.UI.createLabel({
			text: aux +" "+L('beds'),
			textAlign:'center',
			borderColor: 'gray',
			borderRadius: 5,
			borderWidth: 2,
			color: 'gray',
			height: 30,
			font: {
				fontWeight:'bold',
				fontSize: 18,
			},
			width: '40%',
			right: 5,
			top: 10,
		});
		
		tablerow.add(roomLabel);
		tablerow.add(beds);
		return tablerow;
};