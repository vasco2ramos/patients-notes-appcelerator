Ti.include('/ui/common/RoomsRowsConstructor.js');

//Rooms View Component Constructor
function RoomsView() {
	
	var self = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: '85%',
		width:'90%',
		top: 10,
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0'		   
	});
	
	var table = Titanium.UI.createTableView({
		style:Titanium.UI.iPhone.TableViewStyle.PLAIN,
		backgroundColor:'transparent',
		separatorStyle: Ti.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		allowSelection: true,
		backgroundSelectedColor:'black',
		editable: true,
		editing:true,
	});
	
	table.addEventListener('click', function(e) {	
	});
	
	var rows = [];
	
	self.refreshRoomList = function() {	
		var Database = require('services/fio');
		var rooms = Database.getRooms();
		rows = [];
		var size = rooms.length;
		Ti.API.info("Rooms Size - "+ size);
		for(i=0; i<size; i++){
			rows.push(createRoomRow(rooms[i],i));
		}
		table.setData(rows);
	};
	
	table.addEventListener('delete',function(e){
		var Database = require('services/fio');
		// delete all patients here
		Database.deletePatientsByRoom(e.row.data);
		Database.deleteRoom(e.row.data);
		self.fireEvent('roomsChanged');
    });
	
	
	var box = Ti.UI.createView({
		top:100,
		width: '90%',
		height: '50%',
	    backgroundColor:'#2e7ab8',
    });
	
	// Create customization of beds and rooms here
	
	var bedLabel = Ti.UI.createLabel({
		top:10,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		width:'90%',
		color: 'white',
		text: L('add_room'),	
	});
	
	
	var nBedLabel = Ti.UI.createLabel({
		top:40,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:30,
		width: '50%',
		left: '5%',
		color: 'white',
		text: L('n_beds'),	
	});
	
	
	var bed = Ti.UI.createTextField({
		height:30,
		top:40,
		left: '55%',
		width: '15%',
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		color:'#888',
		textAlign:'left',
		keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		autocorrect: false,			
	});
	
	var addBedButton = Titanium.UI.createButton({
		backgroundImage: 'images/add.png',
		width:30,
		height:30,
		top:40,
		left: '75%',
	});
	
	
	addBedButton.addEventListener('click', function(e) {
		var Database = require('services/fio');
		if(typeof bed.value==='number' && (bed.value%1)===0 && bed.value!=0){
			alert(L('insert_int_warning'));
		}
		var dialog = Ti.UI.createAlertDialog({
    		cancel: 1,
    		buttonNames: [L('confirm'), L('cancel')],
    		message: L('create_room_warning'),
    		title: L('create'),
  		});
  		
  		dialog.addEventListener('click', function(e){
    		if (e.index === e.source.cancel){
      			dialog.hide();
    		}
    		else{
				Database.createRoom(bed.value);
				self.fireEvent('roomsChanged');
    		}
  		});
  		dialog.show();
	});
	
	self.add(bedLabel);
	self.add(bed);
	self.add(nBedLabel);
	self.add(addBedButton);
	box.add(table);
	self.add(box);
	
	return self;	
}
module.exports = RoomsView;