/**
 * @author Vasco Ramos
 */

function UserArea() {
	var self = Ti.UI.createWindow({
		title:L('user_area'),
		backgroundColor: '#1C1C1C',
		tintColor: '#0199A6',
	});
	
	var doctorInfoView = Ti.UI.createView({
		width: 320,
		height: 200,
		top: 0,
		left: 0,
		backgroundColor:'white',
	}); 			
	self.add(doctorInfoView);
	
	var hospitalsView = Ti.UI.createView({
		width: 320,
		height: Ti.UI.FILL,
		top: 200,
		left: 0,
		backgroundColor:'blue',
	});
	self.add(hospitalsView);
	
	// Rooms Button
	var roomsView = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		top:0,
		left:320,
		backgroundColor:'red',
	});
	self.add(roomsView);
	
	return self;	
}
module.exports = UserArea;
