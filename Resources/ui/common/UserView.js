var createReceivedRow = function(item) {
	var tablerow = Ti.UI.createTableViewRow({
		height: 50,
		link: item.link,
		className: 'itemRow',
		data: item,
		hasChild: true,
		selectedBackgroundColor: '#c4dcf0',
		backgroundColor:'#e3eef8',
	});
	
	var nameview = Ti.UI.createLabel({
		text: item.name,
		color: '#000',
		height: 20,
		font: {
			fontSize: 16,
		},
		left: 25,
		top: 15,
	});
	
	tablerow.add(nameview);
	return tablerow;
};

var createShareableRow = function(item) {
	var tablerow = Ti.UI.createTableViewRow({
		height: 110,
		link: item.link,
		className: 'itemRow',
		data: item,
		hasChild: true,
		selectedBackgroundColor: '#c4dcf0',
		backgroundColor:'#e3eef8',
	});
	
	var bedview = Ti.UI.createLabel({
		text: item.room+' - '+L('bed')+' '+ item.bed,
		textAlign:'center',
		color: '#000',
		borderColor: 'black',
		borderRadius: 5,
		borderWidth: 2,
		height: 25,
		font: {
			fontWeight:'bold',
			fontSize: 16,
		},
		width: '40%',
		left: 5,
		top: 5,
	});
	
	var genderview = Ti.UI.createLabel({
		color: '#000',
		height: 20,
		textAlign:'center',
		font: {
			fontSize: 14,
		},
		left: 5,
		top: 35,
	});
	
	if(item.sex==0){
		genderview.setText(L('gender_m'));
		genderview.setColor('#007FFF');
	}
	else{
		genderview.setText(L('gender_f'));
		genderview.setColor('#E30B5C');
	}
	
	var nameview = Ti.UI.createLabel({
		text: item.name,
		color: '#000',
		height: 20,
		font: {
			fontSize: 16,
		},
		left: 25,
		top: 35,
	});
	//Calculating age
	var today = new Date();
	var birthdate = item.birthdate;
	//birthdate = birthdate.slice(0,birthdate.indexOf(' '));
	var adm = new Date(birthdate.slice(0,4),birthdate.slice(5,7)-1,birthdate.slice(8,10));
	var diff = today-adm;
	var diffdays = diff / (31557600000);
	var age = Math.floor(diffdays);
		
	var ageview = Ti.UI.createLabel({
		text: age+' '+L('years'),
		textAlign:'left',
		color: '#000',
		height: 20,
		font: {
			fontSize: 16,
		},
		left: 5,
		top: 55,
	});
	
	var admissiondate = item.admissiondate;
	adm = new Date(admissiondate.slice(0,4),admissiondate.slice(5,7)-1,admissiondate.slice(8,10));
	diff = today -adm;
	diffdays = diff / 1000 / (60 * 60 * 24);
	var los = Math.floor(diffdays);
	
	var losview = Ti.UI.createLabel({
		text: los+' '+L('length_stay'),
		color: 'gray',
		width:'50%',
		height: 20,
		textAlign:'right',
		font: {
			fontSize: 14,
		},
		right:10,
		top:5,
	});
	
	var motivoview = Ti.UI.createLabel({
		text: item.reason,
		textAlign:'left',
		color: '#000',
		height: 40,
		font: {
			fontSize: 16,
		},
		left: 5,
		top: 75,
	});
	
	
	var estadoview = Ti.UI.createImageView({
		right: 5,
		top: 75,
		width: 23,
		height: 23,
	});
	
	if(item.state=='stable'){
		estadoview.setImage('images/green.png');
	}
	else if(item.state=='req_atention'){
		estadoview.setImage('images/yellow.png');
	}
	else{
		estadoview.setImage('images/red.png');
	}
	
	tablerow.add(estadoview);
	tablerow.add(genderview);
	tablerow.add(nameview);
	tablerow.add(ageview);
	tablerow.add(bedview);
	tablerow.add(losview);
	tablerow.add(motivoview);
	return tablerow;
};


function UserView() {
	var Database = require('services/fio');
		
	var auth = require('services/authentication');
	
	var self = Ti.UI.createWindow({
		backgroundColor: '#2e7ab8',
		title: L('user_area'),
		barColor: '#4893d1',
		
	});
	
	var win1 = Titanium.UI.iOS.createNavigationWindow({
		tintColor: '#FFFFFF',
    	window : self
	});
	
	var rows = [];
	
	var shareLabel = Ti.UI.createLabel({
		top:10,	
		font:{fontSize:18,fontFamily:'Trebuchet MS', fontWeight:'bold'},
		height:20,
		left:10,
		width:'25%',
		color: 'white',
		text: L('share_patient'),	
	});
	self.add(shareLabel);
	
	var encrypt = Titanium.UI.createTextField({  
	    color:'#336699',  
	    top:10,  
	    left:'30%',  
	    width:'30%',  
	    height:20,  
	    hintText:'password',  
	    keyboardType:Titanium.UI.KEYBOARD_DEFAULT,  
	    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,  
	    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    autocorrect: false,
	    autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	});  
	self.add(encrypt);
	
	var email = Titanium.UI.createTextField({  
	    color:'#336699',  
	    top:10,  
	    left:'65%',  
	    width:'30%',  
	    height:20,  
	    hintText:'email',  
	    keyboardType:Titanium.UI.KEYBOARD_DEFAULT,  
	    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,  
	    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    autocorrect: false,
	    autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	    keyboardType:Titanium.UI.KEYBOARD_EMAIL, 
	});  
	self.add(email);
	
	var patientView = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: '40%',
		width:'90%',
		top: 40,
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0'		   
	});
	
	var myPatientsTable = Titanium.UI.createTableView({
		style:Titanium.UI.iPhone.TableViewStyle.PLAIN,
		backgroundColor:'transparent',
		separatorStyle: Ti.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		allowSelection: true,
		backgroundSelectedColor:'black',
	});
	
	patientView.add(myPatientsTable);
	self.add(patientView);
	
	var receivedPatientView = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: '40%',
		width:'90%',
		top: '55%',
		borderWidth: '2',
		borderRadius: '5',
		borderColor: '#85B7E0'		   
	});
	
	var receivedPatientsTable = Titanium.UI.createTableView({
		style:Titanium.UI.iPhone.TableViewStyle.PLAIN,
		backgroundColor:'transparent',
		separatorStyle: Ti.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		allowSelection: true,
		backgroundSelectedColor:'black',
	});
	
	var decrypt = Titanium.UI.createTextField({  
	    color:'#336699',  
	    top:'50%',  
	    left:'50%',  
	    width:'45%',  
	    height:20,  
	    hintText:'password',  
	    keyboardType:Titanium.UI.KEYBOARD_DEFAULT,  
	    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,  
	    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    autocorrect: false,
	    autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	});  
	self.add(decrypt);
	
	receivedPatientView.add(receivedPatientsTable);
	self.add(receivedPatientView);

	myPatientsTable.addEventListener('click', function(e) {
		if(e.row.data){
			//var bf = new Blowfish(decrypt.value);
			var notes = Database.getNotesById(e.row.data.id);
			//var crypted = bf.encrypt(notes);
			auth.sendPatient(email.value, e.row.data, notes, encrypt.value); 
		}	
	});
	
	receivedPatientsTable.addEventListener('click', function(e) {
		if(e.row.data){
			var patient; 
			var id = Ti.App.addEventListener('continue2', function(e){
			 	patient = e.response;
			 	if(Database.camaOcupada(patient.room,patient.bed)){
			 		Database.deletePatientByLocal(patient.room,patient.bed);
			 	}
			 	alert(patient);
				Database.addPatient(patient);
				// Replace my patient on the database here
				//Ti.App.removeEventListener(id);
	 		});
			patient = auth.getSharedPatient(e.row.data.id, decrypt.value);
		}	
	});
	
	win1.refreshPatientList = function() {
		var patients = Database.getPatients();
		if (Object.prototype.toString.apply(patients) === '[object Array]') {
			for (i = 0; i < patients.length; i++) {
				rows.push(createShareableRow(patients[i]));
			}		
			myPatientsTable.setData(rows);
		}
	};
	
	win1.refreshReceivedPatients = function() {
		var patients; 
		var rows = [];
		var id = Ti.App.addEventListener('continue1', function(e){
			//Ti.App.removeEventListener(id);
		 	patients = e.response;
		 	if (Object.prototype.toString.apply(patients) === '[object Array]') {
				for (i = 0; i < patients.length; i++) {
					rows.push(createReceivedRow(patients[i]));
				}		
				receivedPatientsTable.setData(rows);
			}
 		});
		patients = auth.getPatients();
	};
		
	var cancelButton = Ti.UI.createButton({
		style: Titanium.UI.iPhone.SystemButton.INFO_LIGHT,
		title: 'Cancel',
		color: '#FFFFFF',
	});
	
	cancelButton.addEventListener('click', function(e){
		win1.close();
	});
	
	 
	self.leftNavButton=cancelButton;
	
	return win1;	
}
module.exports = UserView;