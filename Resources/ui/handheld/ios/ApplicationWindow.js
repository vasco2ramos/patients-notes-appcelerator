//Application Window Component Constructor
function ApplicationWindow() {
	//declare module dependencies
	var MasterView = require('ui/common/MasterView'),
		DetailView = require('ui/common/DetailView'),
		NewPatientView = require('ui/common/NewPatientView'),
		NewCommentView = require('ui/common/NewCommentView'),
		AboutView = require('ui/common/AboutView'),
		MenuView = require('ui/common/MenuViewIphone'),
		Database = require('services/fio');
	
	
	// Get Patients Database
	var patients = Database.getPatients();
	var rooms = Database.getOnlyRooms();
	
	//construct UI
	var masterView = new MasterView(),
		detailView = new DetailView(),
		newCommentView;
		
	var navigationWindow = Ti.UI.iOS.createNavigationWindow({
		window : masterView,
		barColor: '#ffffff',
		translucent: true,
		includeOpaqueBars: true,
		statusBarStyle: Titanium.UI.iPhone.StatusBar.GREY
	});

	//----------------------------------------------------------------------------------------
	//------------------------- NEW PATIENT BUTTON--------------------------------------------
	//----------------------------------------------------------------------------------------
	// Adding a new patient
	var newPatientButton = Ti.UI.createButton({
		backgroundImage: 'images/addpatient.png',
		width:32,
		height:32, 
	});
	
	
	// Event listener
	newPatientButton.addEventListener('click', function(e) {
		var newPatientView = new NewPatientView([],1);
		newPatientView.addEventListener('refreshTable', function() {
			masterView.refreshPatientList(rooms);	
		});
		navigationWindow.openWindow(newPatientView, {animated:true});
	});
	
	
	
	//----------------------------------------------------------------------------------------
	//--------------------END OF NEW PATIENT BUTTON-------------------------------------------
	//----------------------------------------------------------------------------------------
	
	
	var detailMenuView;
	var activePatient;
	
	masterView.addEventListener('patientSelected', function(e) {
		Ti.API.log('Adding Menu View');
		detailMenuView = new MenuView(e.data);
		detailView.add(detailMenuView);
		activePatient=e.data.id;
		navigationWindow.openWindow(detailView, {animated:true});
		detailView.showPatientInfo(activePatient);
		
		// The Following code sucks donkey dick	
		detailView.remove(adview);
		adview = Admob.createView({
			publisherId:dfp,
			width:320,
			height:50,
			top:0,
		});	
		detailView.add(adview);
		
		adview.addEventListener('didReceiveAd', function() {
    		detailView.doTop(50);
    		Ti.API.info('Success on receiving ad');
		});
		adview.addEventListener('didFailToReceiveAd', function() {
			Ti.API.info('Fail to receive ad'); 
    		detailView.doTop(0);
		});
	});
	
	
	detailView.addEventListener('addNote', function(e) {
		newCommentView = new NewCommentView(activePatient, []);
		newCommentView.addEventListener('refreshTable', function() {	
			detailView.showPatientInfo(activePatient);	
		});	
		newCommentView.open( {modal:true,modalStyle: Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET});
	});
	
	detailView.addEventListener('changePatient', function(e) {
		var patient = Database.getPatientById(activePatient);
		var newPatientView = new NewPatientView(patient,0);
		newPatientView.addEventListener('refreshTable', function() {
			masterView.refreshPatientList(rooms);	
		});
		newPatientView.open( {modal:true,modalStyle: Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET});
	});
	
	detailView.addEventListener('deletePatient', function(e) {
		var dialog = Ti.UI.createAlertDialog({
    		cancel: 1,
    		buttonNames: [L('confirm'), L('cancel')],
    		message: L('delete_warning'),
    		title: L('delete'),
  		});
  		
  		dialog.addEventListener('click', function(e){
    		if (e.index === e.source.cancel){
      			dialog.hide();
    		}
    		else{
    			detailMenuView.hide();
				detailView.clean();
				Database.deletePatient(activePatient);
				Database.deleteNotesById(activePatient);
				masterView.refreshPatientList(rooms);
    		}
  		});
  		dialog.show();
	});
	
	
	var Admob = require('ti.admob');
	var dfp = '/9815150/PN_Fixed';
	var adview = Admob.createView({
		publisherId:dfp,
		width:320,
		height:50,
		top:0,
	});	
	
	detailView.add(adview);

	
	adview.addEventListener('didReceiveAd', function() {
    	Ti.API.info('Success on receiving ad');
	});
	adview.addEventListener('didFailToReceiveAd', function() {
		Ti.API.info('Fail to receive ad'); 
    	detailView.setTop(0);
	});
		
	masterView.rightNavButton = newPatientButton;
	masterView.refreshPatientList(rooms);
	return navigationWindow;
};
module.exports = ApplicationWindow;