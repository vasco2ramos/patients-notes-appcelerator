function MenuView(patient){
	Ti.API.log('Creating View');
	var menu = Ti.UI.createView({
		backgroundColor: '#2e7ab8',
		height: 80,
		width:'100%',
		bottom: 0,
		left: 0,
    });
    
    
    var info = Titanium.UI.createLabel({
		font:{fontSize:14,fontFamily:'Trebuchet MS'},
		height:20,
		left:10,
		top:5,
		width: Ti.UI.FILL,
		color: 'white',
		text: L('room')+' - '+patient.room+"   "+L('bed')+' - '+patient.bed+"    "+patient.name
	});
	menu.add(info);
	
	var changePatientView = Ti.UI.createView({
		left: 5,
		top:25,
		width: 70,
		height:70,
    });
	
	
	var alterarDoenteImage = Titanium.UI.createImageView({
		image:'/images/editnote.png',
		width:49,
		height:49,
		top:0
	});
	changePatientView.add(alterarDoenteImage);
	
	var changeNoteView = Ti.UI.createView({
		left: 120,
		top:25,
		width: 70,
		height:70,
    });
	
	
	var adicionarNotaImage = Titanium.UI.createImageView({
		image:'/images/addnote.png',
		width:49,
		height:49,
		top:0
	});
	changeNoteView.add(adicionarNotaImage);
	
	var deletePatientView = Ti.UI.createView({
		left: 215,
		top:25,
		width: 70,
		height:70,
    });
	
	var eliminarDoenteImage = Titanium.UI.createImageView({
		image:'/images/deletepatient.png',
		width:49,
		height:49,
		top:0
	});
	deletePatientView.add(eliminarDoenteImage);
	
	changePatientView.addEventListener('click', function(e) {
		menu.fireEvent('changePatient', {});
	});
	changeNoteView.addEventListener('click', function(e) {
		menu.fireEvent('addNote', {});
	});
	deletePatientView.addEventListener('click', function(e) {
		menu.fireEvent('deletePatient', {});
	});


	menu.add(changePatientView);
	menu.add(changeNoteView);
	menu.add(deletePatientView);
	return menu;
}
module.exports = MenuView;