//Application Window Component Constructor
function ApplicationWindow() {
	
	//declare module dependencies
	var HomeView = require('ui/common/HomeView'),
		PatientsSplitView = require('ui/common/PatientsSplitView'),
		UserArea = require('ui/common/UserArea');		
	
	var home = new HomeView(),
		patientsSplitView = new PatientsSplitView(),
		userArea = new UserArea();
		
	// create tab group
	var tabGroup = Titanium.UI.createTabGroup({
	    navBarHidden:true,
	    barColor: '#0199A6',
	});
	Titanium.UI.currentTabGroup = tabGroup;
	//
	// create home tab and root window
	//
	tabGroup.addTab(Titanium.UI.createTab({  
	    icon: 'images/tabs/menu_home.png',
	    title: 'Home',
	    window: home
	}));
	//
	// create users tab and root window
	//
	
	
	tabGroup.addTab(Titanium.UI.createTab({
	    icon: 'images/tabs/menu_patients.png',
	    title: 'Patients',
	    window: patientsSplitView
 	    
	}));
	//
	// create feeds tab and root window
	//
	tabGroup.addTab(Titanium.UI.createTab({  
	    icon:'images/tabs/menu_myarea.png',
	    title:'My Area',
	    window: userArea
	}));

	// open tab group
	return tabGroup;


	/*	
	
	
	//----------------------------------------------------------------------------------------
	//------------------------- NEW PATIENT BUTTON--------------------------------------------
	//----------------------------------------------------------------------------------------
	// Adding a new patient
	var newPatientButton = Ti.UI.createButton({
		backgroundImage: 'images/addpatient.png',
		width:32,
		height:32, 
	});
	
	
	// Event listener
	newPatientButton.addEventListener('click', function(e) {
		var newPatientView = new NewPatientView([],1);
		newPatientView.addEventListener('refreshTable', function() {
			patientsView.refreshPatientList(rooms);	
		});
		newPatientView.open( {modal:true,modalStyle: Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET});
	});
	
	
	
	//----------------------------------------------------------------------------------------
	//--------------------END OF NEW PATIENT BUTTON-------------------------------------------
	//----------------------------------------------------------------------------------------
	
	
	var detailMenuView;
	var activePatient;
	

	
	
	detailView.addEventListener('addNote', function(e) {
		newCommentView = new NewCommentView(activePatient, []);
		newCommentView.addEventListener('refreshTable', function() {	
			detailView.showPatientInfo(activePatient);	
		});	
		newCommentView.open( {modal:true,modalStyle: Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET});
	});
	
	detailView.addEventListener('changePatient', function(e) {
		var patient = Database.getPatientById(activePatient);
		var newPatientView = new NewPatientView(patient,0);
		newPatientView.addEventListener('refreshTable', function() {
			patientsView.refreshPatientList(rooms);	
		});
		newPatientView.open( {modal:true,modalStyle: Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET});
	});
	
	detailView.addEventListener('deletePatient', function(e) {
		var dialog = Ti.UI.createAlertDialog({
    		cancel: 1,
    		buttonNames: [L('confirm'), L('cancel')],
    		message: L('delete_warning'),
    		title: L('delete'),
  		});
  		
  		dialog.addEventListener('click', function(e){
    		if (e.index === e.source.cancel){
      			dialog.hide();
    		}
    		else{
    			detailMenuView.hide();
				detailView.clean();
				Database.deletePatient(activePatient);
				Database.deleteNotesById(activePatient);
				patientsView.refreshPatientList(rooms);
    		}
  		});
  		dialog.show();	
	});
	
	
	detailView.freshStart();
	patientsView.rightNavButton = newPatientButton;
	patientsView.refreshPatientList(rooms);

	
	return self;
	*/
}
module.exports = ApplicationWindow;